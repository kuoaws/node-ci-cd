const express = require('express');

const app = express();

app.use(express.json())

app.use('/', (req, res, next) => {
    res.send({ message: 'i love orange' })
})

app.listen(8080, () => {
    console.log("server start at port 8080")
});
