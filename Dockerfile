# base image
FROM node:19-alpine3.16

# create app directory
WORKDIR /user/src/app

# install app dependencies
COPY package*.json /user/src/app
RUN npm install

# bundle app source
COPY . /user/src/app

EXPOSE 8080

CMD ["node", "index.js"]